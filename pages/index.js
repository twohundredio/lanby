var Metalsmith  = require('metalsmith');
var markdown    = require('metalsmith-markdown');
var layouts     = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');

Metalsmith(__dirname)    
.metadata({                 // add any variable you want
  // use them in layout-files
sitename: "Lanby"
})      // instantiate Metalsmith in the cwd
  .source('./src')        // specify source directory
  .destination('./public')     // specify destination directory
  .use(markdown())             // transpile markdown into html
  .use(layouts({
    engine: 'handlebars',
    default: 'layout.html'
  }))             // wrap a nunjucks layout around the html
  .build(function(err) {       // this is the actual build process
    if (err) throw err;    // throwing errors is required
  });