# Lanby

Get Lanby at [https://gitlab.com/twohundredio/lanby](https://gitlab.com/twohundredio/lanby)

Lanby is a Kubernetes load-balancer that uses the host IP addresses of the cluster nodes as endpoints.
It allows the use of a LoadBalancing Service instead of using a NodePort Service, or setting a hostPort on a Pod.

Lanby is **not intended for production use** as the forwarding pod is not guaranteed to stay on the same node, this changes the external IP on the LoadBalancer Service. It is intended for development and/or testing where LoadBalancer requests are needed to be fulfilled.

## Installation

Install Lanby by applying the manifests in the manifests folder:

```
kubectl apply -f manifests/lanby.yaml
```

To uninstall delete the manifests:

```
kubectl delete -f manifests/lanby.yaml
```

## Usage

Create a Service of type "LoadBalancer", Lanby will create the forwarding pods required to satisfy it.

An example service:

```
apiVersion: v1
kind: Service
metadata:
  labels:
    app: web-server-node
  name: web-server-node
  annotations:
    lanby.twohundred.io/all-nodes: "true"
spec:
  ports:
  - name: webserver
    port: 80
    protocol: TCP
    targetPort: 8080
  selector:
    app: web-server-node
  type: LoadBalancer
```

Lanby can deploy to all nodes or a single node. When deploying to all nodes the service will be available
of all host IPs. Deploying to a single node means it can only be accessed through one IP address, the advantage
of this mode is that it allows you to have services listening on the same port but different nodes.

To deploy to all nodes set the annotation ```lanby.twohundred.io/all-nodes``` to ```true``` as shown in the
example above.

To view the IP address the service is listening on run ```kubectl get service <service-name>```, the address is listed
under ```EXTERNAL-IP```:

```
$ kubectl get service web-server-node
NAME              TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)        AGE
web-server-node   LoadBalancer   10.43.193.24   192.168.88.229   80:32059/TCP   51s
```

