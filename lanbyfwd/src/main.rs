extern crate iptables;
#[macro_use]
extern crate log;

use actix_web::{web, App, HttpRequest, HttpServer, Responder};
use std::env;
use std::fs::OpenOptions;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;

async fn status(_req: HttpRequest) -> impl Responder {
    "OK"
}

struct ForwardingEntry {
    proto: String,
    port: String,
    dst_ip: String,
    dst_port: String,
}

fn set_ip_forward(attempt_set : bool) -> std::io::Result<()> {
    let mut file = OpenOptions::new()
        .write(attempt_set)
        .read(true)
        .open("/proc/sys/net/ipv4/ip_forward")?;

    if attempt_set {
        file.write_all(b"1")?;
    }

    let mut value = String::new();
    file.seek(SeekFrom::Start(0))?;
    file.read_to_string(&mut value)?;

    if value.trim() != "1" {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "ip_forward not set",
        ));
    }

    Ok(())
}

fn set_ip_tables_rules(
    proto: &str,
    port: &str,
    dst_ip: &str,
    dst_port: &str,
) -> Result<(), iptables::error::IPTError> {
    let ipt = iptables::new(false)?;

    // PREROUTING
    let pre_chain = format!("LANBY_{}_{}_PRE", port, proto);
    ipt.new_chain("nat", &pre_chain)?;

    let pre_rule = format!(
        "! -s {}/32 -p {} --dport {} -j DNAT --to {}:{}",
        dst_ip, proto, port, dst_ip, dst_port
    );
    ipt.append("nat", &pre_chain, &pre_rule)?;

    let pre_jump = format!("-j {}", &pre_chain);
    ipt.insert("nat", "PREROUTING", &pre_jump, 1)?;

    // POSTROUTING
    let post_chain = format!("LANBY_{}_{}_POST", port, proto);
    ipt.new_chain("nat", &post_chain)?;

    let post_jump = format!("-j {}", &post_chain);
    ipt.insert("nat", "POSTROUTING", &post_jump, 1)?;

    let post_rule = format!("-d {}/32 -p {} -j MASQUERADE", dst_ip, proto);
    ipt.append("nat", &post_chain, &post_rule)?;

    Ok(())
}

fn read_forwarding_entries() -> Vec<ForwardingEntry> {
    let mut entries: Vec<ForwardingEntry> = Vec::new();
    let mut index = 1;
    let mut done = false;

    while !done {
        let pr_key = format!("PROTO_{}", index);
        let di_key = format!("DST_IP_{}", index);
        let dp_key = format!("DST_PORT_{}", index);
        let po_key = format!("PORT_{}", index);

        match env::var(pr_key) {
            Ok(proto) => {
                let mut valid = true;

                let port = match env::var(po_key) {
                    Ok(v) => v,
                    Err(_) => {
                        valid = false;
                        "".to_string()
                    }
                };
                let dst_ip = match env::var(di_key) {
                    Ok(v) => v,
                    Err(_) => {
                        valid = false;
                        "".to_string()
                    }
                };
                let dst_port = match env::var(dp_key) {
                    Ok(v) => v,
                    Err(_) => {
                        valid = false;
                        "".to_string()
                    }
                };

                if valid {
                    let entry = ForwardingEntry {
                        proto,
                        port,
                        dst_ip,
                        dst_port,
                    };
                    entries.push(entry);
                } else {
                    warn!("Forwarding entry {} invalid", index);
                }
            }
            Err(_) => {
                done = true;
            }
        }
        index += 1;
    }

    entries
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env::set_var("RUST_LOG", "info");
    env_logger::init();

    info!("LanbyFwd Starting");

    match set_ip_forward(false) {
        Ok(_) => info!("ip_forward set to 1"),
        Err(e) => {
            error!("ip forwarding not set: {}", e);
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Initialization Error",
            ));
        }
    };

    let forwarding_entries = read_forwarding_entries();

    for entry in forwarding_entries {
        info!(
            "Setting LB Rule Protocol {} Port {} Destination IP {} Destination Port {}",
            &entry.proto, &entry.port, &entry.dst_ip, &entry.dst_port
        );
        match set_ip_tables_rules(&entry.proto, &entry.port, &entry.dst_ip, &entry.dst_port) {
            Ok(_) => info!(
                "Set LB Rule Protocol {} Port {} Destination IP {} Destination Port {}",
                &entry.proto, &entry.port, &entry.dst_ip, &entry.dst_port
            ),
            Err(e) => {
                error!("Unable to forwarding rule: {}", e);
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Initialization Error",
                ));
            }
        };
    }

    // Status port should be set in env
    let status_port = match env::var("STATUS_PORT") {
        Ok(port) => port,
        Err(_) => "31313".into(),
    };

    info!("Starting health check on port {}", status_port);
    let bind_address = format!("0.0.0.0:{}", status_port);
    HttpServer::new(|| App::new().route("/status", web::get().to(status)))
        .bind(&bind_address)?
        .run()
        .await
}
