use anyhow::Result;
use async_std::task;
use futures::StreamExt;
use std::collections::HashSet;
use std::sync::Arc;
use std::sync::Mutex;
use std::time::Duration;

use k8s_openapi::api::core::v1::{
    LoadBalancerIngress, LoadBalancerStatus, PodSpec, PodStatus, ServiceStatus,
};

use kube::{
    api::{Api, Object, PostParams, WatchEvent},
    client::APIClient,
    runtime::Informer,
};

use crate::lb::OwnerRef;
use crate::lb::LB_POD_LABEL;

type PodUpdate = Object<PodSpec, PodStatus>;

#[derive(PartialEq)]
enum Action {
    Add,
    Remove,
}

struct PodWatcher {
    client: APIClient,
    service_retry: Arc<Mutex<HashSet<OwnerRef>>>,
}

pub async fn monitor_pods(
    client: APIClient,
    service_retry: Arc<Mutex<HashSet<OwnerRef>>>,
) -> Result<()> {
    let mut watcher = PodWatcher::new(client, service_retry);

    loop {
        if let Err(e) = watcher.watch().await {
            error!("Pod watcher error: {}", e);
        }

        warn!("Pod watcher disconnected, will reconnect");
        task::sleep(Duration::from_secs(20)).await;
    }
}

impl PodWatcher {
    pub fn new(client: APIClient, service_retry: Arc<Mutex<HashSet<OwnerRef>>>) -> Self {
        PodWatcher {
            client,
            service_retry,
        }
    }

    pub async fn watch(&mut self) -> Result<()> {
        let pod_api = Api::v1Pod(self.client.clone());
        let pod_inf = Informer::new(pod_api.clone()).labels(LB_POD_LABEL);

        loop {
            let mut pod_list = pod_inf.poll().await?.boxed();

            while let Some(event) = pod_list.next().await {
                let event = event?;
                self.handle_pod(event).await?;
            }
        }
    }

    fn get_service_status_ips(&self, service_status: &Option<ServiceStatus>) -> Vec<String> {
        let mut ips: Vec<String> = Vec::new();

        if let Some(status) = service_status {
            if let Some(load_balancer) = &status.load_balancer {
                if let Some(lb_ingress) = &load_balancer.ingress {
                    for ingress in lb_ingress.iter() {
                        if let Some(ip) = &ingress.ip {
                            ips.push(ip.to_string());
                        }
                    }
                }
            }
        }

        ips
    }

    async fn add_lb_ip(
        &self,
        namespace: String,
        service_name: String,
        uid: String,
        ip: String,
    ) -> anyhow::Result<()> {
        debug!(
            "Add ip: Namespace {} Service {} IP {}",
            &namespace, &service_name, &ip
        );

        let cmapi = Api::v1Service(self.client.clone()).within(&namespace);

        let mut service = cmapi.get(&service_name).await?;
        if let Some(s_id) = &service.metadata.uid {
            if *s_id != uid {
                return Err(anyhow!("Service uid {} does not match {}", s_id, uid));
            }
        } else {
            return Err(anyhow!("No Service uid".to_string()));
        }

        let mut ips = self.get_service_status_ips(&service.status);

        if !ips.contains(&ip) {
            ips.push(ip.clone());

            let mut status = ServiceStatus::default();
            let mut lbs = LoadBalancerStatus::default();
            let ingress = ips
                .into_iter()
                .map(|i| LoadBalancerIngress {
                    hostname: None,
                    ip: Some(i),
                })
                .collect();
            lbs.ingress = Some(ingress);
            status.load_balancer = Some(lbs);

            service.status = Some(status);

            let patch_params = PostParams::default();
            let cm_patched = cmapi
                .replace_status(&service_name, &patch_params, serde_json::to_vec(&service)?)
                .await;

            match cm_patched {
                Ok(_o) => {
                    debug!("Added IP {} to Service {}", &ip, &service_name);
                }
                Err(e) => {
                    debug!(
                        "Error adding IP address to service {}: {:#?}",
                        &service_name, e
                    );
                    return Err(anyhow!("Error adding IP address to service".to_string()));
                }
            }
        }

        Ok(())
    }

    async fn remove_lb_ip(
        &self,
        namespace: String,
        service_name: String,
        uid: String,
        ip: String,
    ) -> anyhow::Result<()> {
        debug!(
            "Remove ip: Namespace {} Service {} IP {}",
            &namespace, &service_name, &ip
        );

        let cmapi = Api::v1Service(self.client.clone()).within(&namespace);

        let mut service = cmapi.get(&service_name).await?;
        if let Some(s_id) = &service.metadata.uid {
            if *s_id != uid {
                return Err(anyhow!("Service uid {} does not match {}", s_id, uid));
            }
        } else {
            return Err(anyhow!("No Service uid".to_string()));
        }

        let ips = self.get_service_status_ips(&service.status);

        if ips.contains(&ip) {
            let mut status = ServiceStatus::default();
            let mut lbs = LoadBalancerStatus::default();
            let ingress = ips
                .into_iter()
                .filter(|i| *i != ip)
                .map(|i| LoadBalancerIngress {
                    hostname: None,
                    ip: Some(i),
                })
                .collect();
            lbs.ingress = Some(ingress);
            status.load_balancer = Some(lbs);

            service.status = Some(status);

            let patch_params = PostParams::default();
            let cm_patched = cmapi
                .replace_status(&service_name, &patch_params, serde_json::to_vec(&service)?)
                .await;

            match cm_patched {
                Ok(_o) => {
                    debug!("Removed IP {} from Service {}", &ip, &service_name);
                }
                Err(e) => {
                    debug!(
                        "Error removing IP address from service {}: {:#?}",
                        &service_name, e
                    );
                    return Err(anyhow!("Error removing IP address from service".to_string()));
                }
            }
        }

        Ok(())
    }

    // Find owner
    async fn get_daemonset_owner(
        &self,
        namespace: String,
        daemonset_name: String,
        uid: String,
    ) -> anyhow::Result<OwnerRef> {
        let api = Api::v1DaemonSet(self.client.clone()).within(&namespace);
        let ds = api.get(&daemonset_name).await?;

        if let Some(ds_id) = &ds.metadata.uid {
            if *ds_id != uid {
                return Err(anyhow!("DaemonSet uid {} does not match {}", ds_id, uid));
            }
        } else {
            return Err(anyhow!("No DaemonSet uid".to_string()));
        }

        if ds.metadata.ownerReferences.len() != 1 {
            error!(
                "DaemonSet has {} owners, should be 1",
                ds.metadata.ownerReferences.len()
            );
            return Err(anyhow!("No DaemonSet owner".to_string()));
        }

        let api_verion = &ds.metadata.ownerReferences[0].apiVersion;
        let name = &ds.metadata.ownerReferences[0].name;
        let uid = &ds.metadata.ownerReferences[0].uid;
        let kind = &ds.metadata.ownerReferences[0].kind;

        Ok(OwnerRef {
            api_version: api_verion.to_string(),
            kind: kind.to_string(),
            name: name.to_string(),
            uid: uid.to_string(),
            namespace,
        })
    }

    async fn get_owner_service(
        &self,
        pod: &PodUpdate,
        get_parent: bool,
    ) -> anyhow::Result<OwnerRef> {
        if pod.metadata.ownerReferences.len() != 1 {
            error!(
                "Pod has {} owners, should be 1",
                pod.metadata.ownerReferences.len()
            );
            return Err(anyhow!("No pod owner".to_string()));
        }

        let namespace = match &pod.metadata.namespace {
            Some(ns) => ns.clone(),
            None => "default".to_string(),
        };

        let api_verion = &pod.metadata.ownerReferences[0].apiVersion;
        let name = &pod.metadata.ownerReferences[0].name;
        let uid = &pod.metadata.ownerReferences[0].uid;
        let kind = &pod.metadata.ownerReferences[0].kind;

        if kind.to_lowercase() == "daemonset" {
            if get_parent {
                self.get_daemonset_owner(namespace.to_string(), name.to_string(), uid.to_string())
                    .await
            } else {
                Err(anyhow!("Pod owner is DaemonSet".to_string()))
            }
        } else {
            Ok(OwnerRef {
                api_version: api_verion.to_string(),
                kind: kind.to_string(),
                name: name.to_string(),
                uid: uid.to_string(),
                namespace,
            })
        }
    }

    fn check_pod_ready(&self, pod: &PodUpdate) -> bool {
        if pod.metadata.deletion_timestamp.is_some() {
            return false;
        }
        if let Some(pod_status) = &pod.status {
            match &pod_status.phase {
                Some(phase) => {
                    if phase.to_lowercase() != "running" {
                        return false;
                    }
                }
                None => {
                    return false;
                }
            };
            if pod_status.host_ip.is_none() {
                return false;
            }
            if let Some(conditions) = &pod_status.conditions {
                for condition in conditions.iter() {
                    if condition.status.to_lowercase() != "true" {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }

        true
    }

    async fn update_pod_to_service(&self, action: Action, pod: &PodUpdate) -> anyhow::Result<()> {
        if !self.check_pod_ip(pod) {
            return Err(anyhow!("No HostIP"));
        }
        let host_ip = pod.status.as_ref().unwrap().host_ip.as_ref().unwrap();

        let namespace = match &pod.metadata.namespace {
            Some(ns) => ns.clone(),
            None => "default".to_string(),
        };

        let owner_service = self.get_owner_service(&pod, true).await;
        match owner_service {
            Ok(owner) => {
                if action == Action::Add {
                    info!(
                        "Adding {} (Namespace: {}) Host IP {} to Service {}",
                        &pod.metadata.name, &namespace, &host_ip, &owner.name
                    );
                    self.add_lb_ip(
                        namespace.to_string(),
                        owner.name,
                        owner.uid,
                        host_ip.to_string(),
                    )
                    .await
                } else if action == Action::Remove {
                    info!(
                        "Removing {} (Namespace: {}) Host IP {} from Service {}",
                        &pod.metadata.name, &namespace, &host_ip, &owner.name
                    );
                    self.remove_lb_ip(
                        namespace.to_string(),
                        owner.name,
                        owner.uid,
                        host_ip.to_string(),
                    )
                    .await
                } else {
                    Err(anyhow!("Unknown update action"))
                }
            }
            Err(_) => Err(anyhow!("Unable to get Pod's owning Service")),
        }
    }

    fn check_pod_ip(&self, pod: &PodUpdate) -> bool {
        if let Some(pod_status) = &pod.status {
            if pod_status.host_ip.is_none() {
                return false;
            }
        } else {
            return false;
        }
        true
    }

    async fn handle_pod(&mut self, ev: WatchEvent<PodUpdate>) -> anyhow::Result<()> {
        match ev {
            WatchEvent::Added(o) => {
                if self.check_pod_ip(&o) {
                    if self.check_pod_ready(&o) {
                        info!("Added Pod Ready: {}", o.metadata.name);

                        match self.update_pod_to_service(Action::Add, &o).await {
                            Ok(_) => {
                                info!("Added {} Host IP to Service", &o.metadata.name);
                            }
                            Err(e) => {
                                warn!(
                                    "Failed Adding {} Host IP to Service: {}",
                                    &o.metadata.name, e
                                );
                            }
                        }
                    } else {
                        info!("Added Pod Not Ready: {}", o.metadata.name);
                        match self.update_pod_to_service(Action::Remove, &o).await {
                            Ok(_) => {
                                info!("Removed {} Host IP from Service", &o.metadata.name);
                            }
                            Err(e) => {
                                warn!(
                                    "Failed removing {} Host IP from Service: {}",
                                    &o.metadata.name, e
                                );
                            }
                        }
                    }
                } else {
                    warn!(
                        "Added Pod No Host IP: {}, cannot update service",
                        o.metadata.name
                    )
                }
            }
            WatchEvent::Modified(o) => {
                if self.check_pod_ip(&o) {
                    if self.check_pod_ready(&o) {
                        info!("Modified Pod Ready: {}", o.metadata.name);

                        match self.update_pod_to_service(Action::Add, &o).await {
                            Ok(_) => {
                                info!("Added {} Host IP to Service", &o.metadata.name);
                            }
                            Err(e) => {
                                warn!(
                                    "Failed Adding {} Host IP to Service: {}",
                                    &o.metadata.name, e
                                );
                            }
                        }
                    } else {
                        info!("Modified Pod Not Ready: {}", o.metadata.name);
                        match self.update_pod_to_service(Action::Remove, &o).await {
                            Ok(_) => {
                                info!("Removed {} Host IP from Service", &o.metadata.name);
                            }
                            Err(e) => {
                                warn!(
                                    "Failed removing {} Host IP from Service: {}",
                                    &o.metadata.name, e
                                );
                            }
                        }
                    }
                } else {
                    warn!(
                        "Modified Pod No Host IP: {}, cannot update service",
                        o.metadata.name
                    )
                }
            }
            WatchEvent::Deleted(o) => {
                let namespace = match &o.metadata.namespace {
                    Some(ns) => ns.clone(),
                    None => "default".to_string(),
                };
                info!("Deleted Pod: {} Namespace: {}", o.metadata.name, namespace);

                if self.check_pod_ip(&o) {
                    match self.update_pod_to_service(Action::Remove, &o).await {
                        Ok(_) => {
                            info!("Removed {} Host IP from Service", &o.metadata.name);
                        }
                        Err(e) => {
                            warn!(
                                "Failed removing {} Host IP from Service: {}",
                                &o.metadata.name, e
                            );
                        }
                    }
                } else {
                    warn!(
                        "Deleted Pod No Host IP: {}, cannot update service",
                        o.metadata.name
                    )
                }

                match self.get_owner_service(&o, false).await {
                    Ok(service) => {
                        if service.kind.to_lowercase() == "service" {
                            match self.service_retry.lock() {
                                Ok(mut sr) => {
                                    sr.insert(service);
                                }
                                Err(e) => {
                                    error!("Cannot add Pod to retry list: {}", e);
                                }
                            };
                        }
                    }
                    Err(e) => {
                        debug!("Cannot get Pod owner for retry: {}", e);
                    }
                };
            }
            WatchEvent::Error(e) => {
                info!("Pod error event: {:?}", e);
            }
        }
        Ok(())
    }
}
