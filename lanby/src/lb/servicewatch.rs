use anyhow::Result;
use async_std::task;
use futures::StreamExt;
use std::collections::BTreeMap;
use std::collections::HashSet;
use std::env;
use std::sync::Arc;
use std::sync::Mutex;
use std::time::Duration;

use k8s_openapi::api::core::v1::{
    Capabilities, Container, ContainerPort, EnvVar, HTTPGetAction, Pod, PodSpec, PodStatus, Probe,
    SecurityContext, ServiceSpec, ServiceStatus,
};

use k8s_openapi::api::apps::v1 as apps;
use k8s_openapi::api::core::v1 as core;
use k8s_openapi::apimachinery::pkg::api::resource::Quantity;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::LabelSelector;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::OwnerReference;
use kube::{
    api::{Api, Object, PostParams, WatchEvent},
    client::APIClient,
    runtime::Informer,
};

use crate::lb::OwnerRef;
use crate::lb::LB_POD_DEFAULT_IMAGE;
use crate::lb::LB_POD_LABEL;

type LoadBalancer = Object<ServiceSpec, ServiceStatus>;
type PodObject = Object<PodSpec, PodStatus>;

struct PortForward {
    protocol: String,
    cluster_ip: String,
    port: i32,
}

struct ServiceWatcher {
    client: APIClient,
    service_retry: Arc<Mutex<HashSet<OwnerRef>>>,
}

pub async fn monitor_service(
    client: APIClient,
    service_retry: Arc<Mutex<HashSet<OwnerRef>>>,
) -> Result<()> {
    let mut watcher = ServiceWatcher::new(client, service_retry);

    loop {
        if let Err(e) = watcher.watch().await {
            error!("Service watcher error: {}", e);
        }

        warn!("Service watcher disconnected, will reconnect");
        task::sleep(Duration::from_secs(20)).await;
    }
}

impl ServiceWatcher {
    pub fn new(client: APIClient, service_retry: Arc<Mutex<HashSet<OwnerRef>>>) -> Self {
        ServiceWatcher {
            client,
            service_retry,
        }
    }

    pub async fn watch(&mut self) -> Result<()> {
        let api = Api::v1Service(self.client.clone());
        let inf = Informer::new(api.clone()).timeout(20);

        loop {
            let mut ingress_list = inf.poll().await?.boxed();

            while let Some(event) = ingress_list.next().await {
                let event = event?;
                self.handle_service(&api, event).await?;
            }

            // Service retry (on user delete or forwarding creation failure)
            let retry_list = match self.service_retry.lock() {
                Ok(mut sr) => {
                    let rl: Vec<OwnerRef> = sr.iter().cloned().collect();
                    sr.clear();
                    Some(rl)
                }
                Err(e) => {
                    error!("Cannot process retry list: {}", e);
                    None
                }
            };
            if let Some(rl) = retry_list {
                if let Err(e) = self.process_retry_list(&rl).await {
                    warn!("Error processing retry list: {}", e);
                }
            }
        }
    }

    async fn process_retry_list(&self, retry_list: &[OwnerRef]) -> Result<()> {
        for retry_service in retry_list.iter() {
            info!("Service retry {}", retry_service.name);
            if let Err(e) = self.process_retry_entry(retry_service).await {
                info!("Could not retry {}: {}", retry_service.name, e);
            }
        }

        Ok(())
    }

    async fn process_retry_entry(&self, retry_service: &OwnerRef) -> Result<()> {
        let cmapi = Api::v1Service(self.client.clone()).within(&retry_service.namespace);
        let service = cmapi.get(&retry_service.name).await?;

        if let Some(s_id) = &service.metadata.uid {
            if *s_id != retry_service.uid {
                return Err(anyhow!(
                    "Service uid {} does not match {}",
                    s_id,
                    retry_service.uid
                ));
            }
            self.create_forwarder(service).await
        } else {
            Err(anyhow!("No Service uid".to_string()))
        }
    }

    fn get_pod_spec(&self, name: &str, fwd_ports: Vec<PortForward>) -> anyhow::Result<PodSpec> {
        let lanbyfwd_image = match env::var("LANBYFWD_IMAGE") {
            Ok(image) => image,
            Err(_) => LB_POD_DEFAULT_IMAGE.into(),
        };

        // Spec
        let mut pod_spec: PodSpec = Default::default();

        pod_spec.dns_policy = Some("ClusterFirst".to_string());
        pod_spec.restart_policy = Some("Always".to_string());
        pod_spec.automount_service_account_token = Some(false);

        let mut container: Container = Default::default();
        container.name = name.to_string();
        container.image = Some(lanbyfwd_image);
        container.image_pull_policy = Some("Always".to_string());

        let mut security_context: SecurityContext = Default::default();
        let mut capabilities: Capabilities = Default::default();
        capabilities.add = Some(vec!["NET_ADMIN".to_string()]);
        security_context.capabilities = Some(capabilities);
        container.security_context = Some(security_context);

        // Ports and Env
        let mut used_ports = HashSet::new();
        let mut envs: Vec<EnvVar> = Vec::new();
        let mut ports: Vec<ContainerPort> = Vec::new();

        let mut index: u32 = 1;
        for fwdport in fwd_ports {
            used_ports.insert(fwdport.port);

            let mut env_port: EnvVar = Default::default();
            env_port.name = format!("PORT_{}", index);
            env_port.value = Some(fwdport.port.to_string());
            envs.push(env_port);

            let mut env_proto: EnvVar = Default::default();
            env_proto.name = format!("PROTO_{}", index);
            env_proto.value = Some(fwdport.protocol.clone());
            envs.push(env_proto);

            let mut env_dst_port: EnvVar = Default::default();
            env_dst_port.name = format!("DST_PORT_{}", index);
            env_dst_port.value = Some(fwdport.port.to_string());
            envs.push(env_dst_port);

            let mut env_dst_ip: EnvVar = Default::default();
            env_dst_ip.name = format!("DST_IP_{}", index);
            env_dst_ip.value = Some(fwdport.cluster_ip.clone());
            envs.push(env_dst_ip);

            let mut host_port: ContainerPort = Default::default();
            host_port.container_port = fwdport.port;
            host_port.host_port = Some(fwdport.port);
            host_port.name = Some(format!(
                "lb-{}-{}",
                &fwdport.protocol.to_lowercase(),
                fwdport.port
            ));
            host_port.protocol = Some(fwdport.protocol.clone());

            ports.push(host_port);

            index += 1;
        }

        // Find a free port for status
        let mut status_port = -1;
        for i in (1025..31314).rev() {
            if !used_ports.contains(&i) {
                status_port = i;
                break;
            }
        }
        if status_port == -1 {
            return Err(anyhow!("Could not find free port for status"));
        }

        let mut container_status_port: ContainerPort = Default::default();
        container_status_port.container_port = status_port;
        container_status_port.name = Some("status-port".to_string());
        container_status_port.protocol = Some("TCP".to_string());
        ports.push(container_status_port);

        envs.push(EnvVar {
            name: "STATUS_PORT".to_string(),
            value: Some(status_port.to_string()),
            value_from: None,
        });

        envs.push(EnvVar {
            name: "LANBYFWD".to_string(),
            value: Some("1".to_string()),
            value_from: None,
        });

        envs.push(EnvVar {
            name: "RUST_BACKTRACE".to_string(),
            value: Some("1".to_string()),
            value_from: None,
        });

        container.env = Some(envs);
        container.ports = Some(ports);

        // Probe
        let mut liveness_probe: Probe = Default::default();
        liveness_probe.initial_delay_seconds = Some(10);
        liveness_probe.timeout_seconds = Some(1);
        let mut probe_action: HTTPGetAction = Default::default();
        probe_action.path = Some("/status".to_string());
        probe_action.port = k8s_openapi::apimachinery::pkg::util::intstr::IntOrString::String(
            "status-port".to_string(),
        );
        liveness_probe.http_get = Some(probe_action);

        container.liveness_probe = Some(liveness_probe);

        // Resource requests
        let mut resources: core::ResourceRequirements = Default::default();

        let mut requests: BTreeMap<String, Quantity> = BTreeMap::new();
        requests.insert("cpu".to_string(), Quantity("10m".to_string()));
        requests.insert("memory".to_string(), Quantity("64Mi".to_string()));

        let mut limits: BTreeMap<String, Quantity> = BTreeMap::new();
        limits.insert("cpu".to_string(), Quantity("250m".to_string()));
        limits.insert("memory".to_string(), Quantity("256Mi".to_string()));

        resources.requests = Some(requests);
        resources.limits = Some(limits);
        container.resources = Some(resources);

        // Put it together
        let mut containers: Vec<Container> = Vec::new();
        containers.push(container);
        pod_spec.containers = containers;

        Ok(pod_spec)
    }

    async fn create_daemon_set(
        &self,
        namespace: String,
        owner: OwnerRef,
        fwd_ports: Vec<PortForward>,
    ) -> anyhow::Result<()> {
        let name = format!("lanbyfwd-{}", &owner.name);

        if self.daemonset_exists(&name, &owner).await {
            info!(
                "Fowarding DaemonSet {} Namespace {} already exists",
                name, namespace
            );
            return Ok(());
        }

        // Create DaemonSet
        debug!("Creating DaemonSet");
        let mut daemon_set: apps::DaemonSet = Default::default();

        // Metadata
        let mut metadata: ObjectMeta = Default::default();
        metadata.name = Some(name.clone());
        metadata.namespace = Some(namespace.clone());

        let mut labels: BTreeMap<String, String> = BTreeMap::new();
        labels.insert(LB_POD_LABEL.to_string(), "true".to_string());
        metadata.labels = Some(labels);

        let mut service_reference: OwnerReference = Default::default();
        service_reference.api_version = owner.api_version;
        service_reference.kind = owner.kind;
        service_reference.controller = Some(true);
        service_reference.name = owner.name.clone();
        service_reference.uid = owner.uid;

        metadata.owner_references = Some(vec![service_reference]);

        daemon_set.metadata = Some(metadata);

        // Spec
        let mut daemon_set_spec: apps::DaemonSetSpec = Default::default();

        // Selector
        let mut match_labels: BTreeMap<String, String> = BTreeMap::new();
        match_labels.insert(LB_POD_LABEL.to_string(), "true".to_string());
        match_labels.insert("name".to_string(), name.clone());
        let mut selector: LabelSelector = Default::default();
        selector.match_labels = Some(match_labels);
        daemon_set_spec.selector = selector;

        // Pod Template
        let mut pod: core::PodTemplateSpec = Default::default();
        let mut template_metadata: ObjectMeta = Default::default();
        let mut labels: BTreeMap<String, String> = BTreeMap::new();
        labels.insert(LB_POD_LABEL.to_string(), "true".to_string());
        labels.insert("name".to_string(), name.clone());
        template_metadata.labels = Some(labels);
        pod.metadata = Some(template_metadata);

        let pod_spec = self.get_pod_spec(&name, fwd_ports)?;
        pod.spec = Some(pod_spec);

        // Add template to DaemonSet
        daemon_set_spec.template = pod;
        daemon_set.spec = Some(daemon_set_spec);

        // Create
        let kapi = Api::v1DaemonSet(self.client.clone()).within(&namespace);
        let pp = PostParams::default();
        match kapi.create(&pp, serde_json::to_vec(&daemon_set)?).await {
            Ok(o) => {
                debug!("Created DaemonSet {}", o.metadata.name);
            }
            Err(e) => return Err(e.into()),
        }

        Ok(())
    }

    async fn daemonset_exists(&self, daemonset_name: &str, owner: &OwnerRef) -> bool {
        let cmapi = Api::v1DaemonSet(self.client.clone()).within(&owner.namespace);

        if let Ok(ds) = cmapi.get(&daemonset_name).await {
            for reference in ds.metadata.ownerReferences.iter() {
                if reference.uid == owner.uid {
                    return true;
                }
            }
        }

        false
    }

    async fn pod_exists(&self, pod_name: &str, owner: &OwnerRef) -> Option<PodObject> {
        let cmapi = Api::v1Pod(self.client.clone()).within(&owner.namespace);

        if let Ok(pod) = cmapi.get(&pod_name).await {
            for reference in pod.metadata.ownerReferences.iter() {
                if reference.uid == owner.uid {
                    return Some(pod);
                }
            }
        }

        None
    }

    async fn create_pod(
        &self,
        namespace: String,
        owner: OwnerRef,
        fwd_ports: Vec<PortForward>,
    ) -> anyhow::Result<()> {
        // Manage pods
        let name = format!("lanbyfwd-{}", &owner.name);

        // Check if it exists
        if let Some(_current_pod) = self.pod_exists(&name, &owner).await {
            info!(
                "Fowarding Pod {} Namespace {} already exists",
                name, namespace
            );
            return Ok(());
        }

        // Create Pod blog
        debug!("Creating Pod");
        let mut pod: Pod = Default::default();

        // Metadata
        let mut metadata: ObjectMeta = Default::default();
        metadata.name = Some(name.clone());
        metadata.namespace = Some(namespace.clone());

        let mut labels: BTreeMap<String, String> = BTreeMap::new();
        labels.insert(LB_POD_LABEL.to_string(), "true".to_string());
        metadata.labels = Some(labels);

        let mut service_reference: OwnerReference = Default::default();
        service_reference.api_version = owner.api_version;
        service_reference.kind = owner.kind;
        service_reference.controller = Some(true);
        service_reference.name = owner.name.clone();
        service_reference.uid = owner.uid;

        metadata.owner_references = Some(vec![service_reference]);

        pod.metadata = Some(metadata);

        // Spec
        let pod_spec = self.get_pod_spec(&name, fwd_ports)?;

        // Add spec
        pod.spec = Some(pod_spec);

        // Create
        let pods = Api::v1Pod(self.client.clone()).within(&namespace);
        let pp = PostParams::default();
        match pods.create(&pp, serde_json::to_vec(&pod)?).await {
            Ok(o) => {
                debug!("Created {}", o.metadata.name);
            }
            Err(e) => return Err(e.into()),
        }

        Ok(())
    }

    fn get_forward_ports(&self, service: &LoadBalancer) -> anyhow::Result<Vec<PortForward>> {
        let mut fwd_ports: Vec<PortForward> = Vec::new();

        if let Some(cluster_ip) = &service.spec.cluster_ip {
            if let Some(ports) = &service.spec.ports {
                for port in ports.iter() {
                    let pf = PortForward {
                        protocol: match &port.protocol {
                            Some(value) => value.to_string(),
                            None => "TCP".to_string(),
                        },
                        cluster_ip: cluster_ip.clone(),
                        port: port.port,
                    };
                    fwd_ports.push(pf);
                }
            } else {
                return Err(anyhow!("Service has no port entries"));
            }
        } else {
            return Err(anyhow!("No cluster ip in service"));
        }

        Ok(fwd_ports)
    }

    fn add_service_to_rety(&self, service: OwnerRef) -> anyhow::Result<()> {
        match self.service_retry.lock() {
            Ok(mut sr) => {
                sr.insert(service);
                Ok(())
            }
            Err(e) => Err(anyhow!("Cannot add Service to retry list: {}", e)),
        }
    }

    async fn create_forwarder(&self, o: LoadBalancer) -> anyhow::Result<()> {
        if o.spec.type_ != Some("LoadBalancer".to_string()) {
            return Ok(());
        }
        if o.metadata.deletion_timestamp.is_some() {
            return Ok(());
        }

        let namespace = match &o.metadata.namespace {
            Some(value) => value.to_string(),
            None => "default".to_string(),
        };
        info!(
            "Service Added: {} Namespace: {}",
            o.metadata.name, &namespace
        );
        if o.metadata.uid.is_none() {
            error!("No uid for service {}", o.metadata.name);
            return Ok(());
        }

        let owner = OwnerRef {
            api_version: "v1".to_string(),
            kind: "Service".to_string(),
            name: o.metadata.name.clone(),
            uid: o.metadata.uid.as_ref().unwrap().clone(),
            namespace: namespace.clone(),
        };

        match self.get_forward_ports(&o) {
            Ok(fwd_ports) => {
                let daemon_set = match o.metadata.annotations.get("lanby.twohundred.io/all-nodes") {
                    Some(value) => value.to_lowercase() == "true",
                    None => false,
                };

                if daemon_set {
                    match self
                        .create_daemon_set(namespace.clone(), owner.clone(), fwd_ports)
                        .await
                    {
                        Ok(_) => {
                            info!(
                                "Created Forwarding DaemonSet for {} (Namespace: {})",
                                &o.metadata.name, &namespace
                            );
                        }
                        Err(e) => {
                            warn!(
                                "Failed created Forwarding DaemonSet for {} (Namespace: {}) Error: {}",
                                &o.metadata.name, &namespace, e
                            );
                            if let Err(e) = self.add_service_to_rety(owner) {
                                error!("Cannot add Service to retry list: {}", e);
                            } else {
                                info!("Will retry forwarder creation");
                            }
                        }
                    }
                } else {
                    match self
                        .create_pod(namespace.clone(), owner.clone(), fwd_ports)
                        .await
                    {
                        Ok(_) => {
                            info!(
                                "Created Forwarding Pod for {} (Namespace: {})",
                                &o.metadata.name, &namespace
                            );
                        }
                        Err(e) => {
                            warn!(
                                "Failed created Forwarding Pod for {} (Namespace: {}) Error: {}",
                                &o.metadata.name, &namespace, e
                            );
                            if let Err(e) = self.add_service_to_rety(owner) {
                                error!("Cannot add Service to retry list: {}", e);
                            } else {
                                info!("Will retry forwarder creation");
                            }
                        }
                    }
                }
            }
            Err(e) => {
                warn!(
                    "Failed created Forwarding for {} (Namespace: {}) Error: {}",
                    &o.metadata.name, &namespace, e
                );
            }
        }

        Ok(())
    }

    // Watch for changes in service objects
    async fn handle_service(
        &self,
        _services: &Api<LoadBalancer>,
        ev: WatchEvent<LoadBalancer>,
    ) -> anyhow::Result<()> {
        match ev {
            WatchEvent::Added(o) => {
                if let Err(e) = self.create_forwarder(o).await {
                    debug!("Error creating forwarder: {}", e);
                }
            }
            WatchEvent::Modified(o) => {
                if o.spec.type_ != Some("LoadBalancer".to_string()) {
                    return Ok(());
                }
                let namespace = o
                    .metadata
                    .namespace
                    .unwrap_or_else(|| "default".to_string());

                info!(
                    "Service Modified: {} Namespace: {}",
                    o.metadata.name, namespace
                );
            }
            WatchEvent::Deleted(o) => {
                if o.spec.type_ != Some("LoadBalancer".to_string()) {
                    return Ok(());
                }
                let namespace = o
                    .metadata
                    .namespace
                    .unwrap_or_else(|| "default".to_string());

                info!(
                    "Service Deleted: {} Namespace: {}",
                    o.metadata.name, namespace
                );
            }
            WatchEvent::Error(e) => {
                warn!("Error event: {:?}", e);
            }
        }
        Ok(())
    }
}
