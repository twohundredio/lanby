pub mod daemonsetwatch;
pub mod podwatch;
pub mod servicewatch;

static LB_POD_LABEL: &str = "lanby.twohundred.io/lanbyfwd";
static LB_POD_DEFAULT_IMAGE: &str = "registry.gitlab.com/twohundredio/lanby:latest";

#[derive(Hash, PartialEq, Eq, Debug, Clone)]
pub struct OwnerRef {
    api_version: String,
    kind: String,
    name: String,
    uid: String,
    namespace: String,
}
