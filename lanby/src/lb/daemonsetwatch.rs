use anyhow::Result;
use async_std::task;
use futures::StreamExt;
use std::collections::HashSet;
use std::sync::Arc;
use std::sync::Mutex;
use std::time::Duration;

use k8s_openapi::api::apps::v1::{DaemonSetSpec, DaemonSetStatus};

use kube::{
    api::{Api, Object, WatchEvent},
    client::APIClient,
    runtime::Informer,
};

use crate::lb::OwnerRef;
use crate::lb::LB_POD_LABEL;

type DaemonSetUpdate = Object<DaemonSetSpec, DaemonSetStatus>;

struct DaemonSetWatcher {
    client: APIClient,
    service_retry: Arc<Mutex<HashSet<OwnerRef>>>,
}

pub async fn monitor_daemonsets(
    client: APIClient,
    service_retry: Arc<Mutex<HashSet<OwnerRef>>>,
) -> Result<()> {
    let mut watcher = DaemonSetWatcher::new(client, service_retry);

    loop {
        if let Err(e) = watcher.watch().await {
            error!("DaemonSet watcher error: {}", e);
        }

        warn!("DaemonSet watcher disconnected, will reconnect");
        task::sleep(Duration::from_secs(20)).await;
    }
}

impl DaemonSetWatcher {
    pub fn new(client: APIClient, service_retry: Arc<Mutex<HashSet<OwnerRef>>>) -> Self {
        DaemonSetWatcher {
            client,
            service_retry,
        }
    }

    async fn watch(&mut self) -> Result<()> {
        let ds_api = Api::v1DaemonSet(self.client.clone());
        let ds_inf = Informer::new(ds_api.clone()).labels(LB_POD_LABEL);

        loop {
            let mut ds_list = ds_inf.poll().await?.boxed();

            while let Some(event) = ds_list.next().await {
                let event = event?;
                self.handle_daemonset(event).await?;
            }
        }
    }

    async fn handle_daemonset(&mut self, ev: WatchEvent<DaemonSetUpdate>) -> anyhow::Result<()> {
        match ev {
            WatchEvent::Added(o) => {
                let namespace = o
                    .metadata
                    .namespace
                    .unwrap_or_else(|| "default".to_string());

                debug!(
                    "DeamonSet Added: {} Namespace: {}",
                    o.metadata.name, namespace
                );
            }
            WatchEvent::Modified(o) => {
                let namespace = o
                    .metadata
                    .namespace
                    .unwrap_or_else(|| "default".to_string());

                debug!(
                    "DeamonSet Modified: {} Namespace: {}",
                    o.metadata.name, namespace
                );
            }
            WatchEvent::Deleted(o) => {
                let namespace = match &o.metadata.namespace {
                    Some(ns) => ns.clone(),
                    None => "default".to_string(),
                };

                info!(
                    "DeamonSet Deleted: {} Namespace: {}",
                    o.metadata.name, namespace
                );

                match self.get_owner_service(&o) {
                    Ok(service) => {
                        match self.service_retry.lock() {
                            Ok(mut sr) => {
                                sr.insert(service);
                            }
                            Err(e) => {
                                error!("Cannot add DaemonSet to retry list: {}", e);
                            }
                        };
                    }
                    Err(e) => {
                        error!("Cannot get DaemonSet owner: {}", e);
                    }
                };
            }
            WatchEvent::Error(e) => {
                info!("DaemonSet error event: {:?}", e);
            }
        }
        Ok(())
    }

    fn get_owner_service(&self, daemonset: &DaemonSetUpdate) -> anyhow::Result<OwnerRef> {
        if daemonset.metadata.ownerReferences.len() != 1 {
            error!(
                "DaemonSet has {} owners, should be 1",
                daemonset.metadata.ownerReferences.len()
            );
            return Err(anyhow!("No DaemonSet owner".to_string()));
        }

        let namespace = match &daemonset.metadata.namespace {
            Some(ns) => ns.clone(),
            None => "default".to_string(),
        };

        let api_verion = &daemonset.metadata.ownerReferences[0].apiVersion;
        let name = &daemonset.metadata.ownerReferences[0].name;
        let uid = &daemonset.metadata.ownerReferences[0].uid;
        let kind = &daemonset.metadata.ownerReferences[0].kind;

        if kind.to_lowercase() == "service" {
            Ok(OwnerRef {
                api_version: api_verion.to_string(),
                kind: kind.to_string(),
                name: name.to_string(),
                uid: uid.to_string(),
                namespace,
            })
        } else {
            Err(anyhow!("DaemonSet owner is not a service".to_string()))
        }
    }
}
