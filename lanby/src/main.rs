#[macro_use]
extern crate log;
#[macro_use]
extern crate anyhow;

use actix_web::{web, App, HttpRequest, HttpServer, Responder};
use kube::client::APIClient;
use std::collections::HashSet;
use std::env;
use std::sync::Arc;
use std::sync::Mutex;

mod lb;
use lb::daemonsetwatch;
use lb::podwatch;
use lb::servicewatch;
use lb::OwnerRef;

async fn status(_req: HttpRequest) -> impl Responder {
    "OK"
}

async fn start_lb_monitor() -> std::io::Result<()> {
    let config = if let Ok(c) = kube::config::incluster_config() {
        c
    } else {
        kube::config::load_kube_config()
            .await
            .expect("Failed to load kube config")
    };

    let service_retry = Arc::new(Mutex::new(HashSet::<OwnerRef>::new()));

    let service_client = APIClient::new(config);
    let pod_client = service_client.clone();
    let pod_service_retry = service_retry.clone();
    let daemonset_client = service_client.clone();
    let ds_service_retry = service_retry.clone();
    tokio::spawn(async move { podwatch::monitor_pods(pod_client, pod_service_retry).await });
    tokio::spawn(async move {
        servicewatch::monitor_service(service_client, service_retry.clone()).await
    });
    tokio::spawn(async move {
        daemonsetwatch::monitor_daemonsets(daemonset_client, ds_service_retry).await
    });

    Ok(())
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env::set_var("RUST_LOG", "info");

    env_logger::init();

    match start_lb_monitor().await {
        Ok(_) => debug!("Started loadbalancer monitoring"),
        Err(e) => {
            error!("Unable to start loadbalancer monitoring: {}", e);
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Initialization Error: {}", e),
            ));
        }
    };

    // Port should be set in env
    let status_port = match env::var("STATUS_PORT") {
        Ok(port) => port,
        Err(_) => "31313".into(),
    };

    info!("Starting health check on port {}", status_port);
    let bind_address = format!("0.0.0.0:{}", status_port);
    HttpServer::new(|| App::new().route("/status", web::get().to(status)))
        .bind(&bind_address)?
        .run()
        .await
}
