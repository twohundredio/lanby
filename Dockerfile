FROM ubuntu:bionic

RUN \
  apt-get update && \
  apt-get install -y iptables libssl1.1 && \
  rm -rf /var/lib/apt/lists/*

COPY target/release/lanbyfwd /lanbyfwd
COPY target/release/lanby /lanby
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
